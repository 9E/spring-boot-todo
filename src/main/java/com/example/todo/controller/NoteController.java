package com.example.todo.controller;

import com.example.todo.model.Note;
import com.example.todo.model.Note;
import com.example.todo.service.NoteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/notes")
public class NoteController {
    private final NoteService noteService;

    public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping
    public List<Note> getNotes() {
        return noteService.findAll();
    }

    @GetMapping("/{id}")
    public Note getNote(@PathVariable Long id) {
        return noteService.findById(id)
                .orElseThrow(() -> new RuntimeException("hi"));
    }

    @PostMapping
    public Note postNote(@RequestBody Note user) {
        return noteService.save(user);
    }

    @PutMapping("/{id}")
    public Note putNote(@PathVariable Long id, @RequestBody Note newNote) {
        return noteService.findById(id)
                .map(note -> {
                    note.setName(newNote.getName());
                    note.setText(newNote.getText());
                    note.setUser(newNote.getUser());
                    return noteService.save(note);
                })
                .orElseThrow(() -> {
                    throw new RuntimeException("hello");
                });
    }

    @DeleteMapping("/{id}")
    public void deleteNote(@PathVariable Long id) {
        if (!noteService.existsById(id)) {
            throw new RuntimeException("bye");
        }

        noteService.delete(id);
    }
}
