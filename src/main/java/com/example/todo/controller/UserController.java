package com.example.todo.controller;

import com.example.todo.model.User;
import com.example.todo.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getUsers() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable Long id) {
        return userService.findById(id)
                .orElseThrow(() -> new RuntimeException("hi"));
    }

    @PostMapping
    public User postUser(@RequestBody User user) {
        return userService.save(user);
    }

    @PutMapping("/{id}")
    public User putUser(@PathVariable Long id, @RequestBody User newUser) {
        return userService.findById(id)
                .map(user -> {
                    user.setUsername(newUser.getUsername());
                    user.setPassword(newUser.getPassword());
                    return userService.save(user);
                })
                .orElseThrow(() -> {
                    throw new RuntimeException("hello");
                });
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        if (!userService.existsById(id)) {
            throw new RuntimeException("bye");
        }

        userService.delete(id);
    }
}
