package com.example.todo.service;

import com.example.todo.model.Note;
import com.example.todo.model.Note;
import com.example.todo.repository.NoteRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NoteService {
    private final NoteRepository noteRepository;

    public NoteService(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    public List<Note> findAll() {
        return noteRepository.findAll();
    }

    public Optional<Note> findById(Long id) {
        return noteRepository.findById(id);
    }

    public boolean existsById(Long id) {
        return noteRepository.existsById(id);
    }

    public Note save(Note user) {
        return noteRepository.save(user);
    }

    public void delete(Long id) {
        noteRepository.deleteById(id);
    }
}
